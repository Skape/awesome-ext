import Vue from 'vue'
import root from './root.vue'
import router from './router'
import firebase from '../backend/firebase.js'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.config.productionTip = false

Vue.use(ElementUI)
let app = false;

firebase.auth().onAuthStateChanged(function (user) {

    if (!app) {
        
        /* eslint-disable no-new */
        app = new Vue({
            el: '#root',
            router,
            render: h => h(root)
        })
    }
})


