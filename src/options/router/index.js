import Vue from 'vue'
import Router from 'vue-router'

import firebase from '../../backend/firebase.js'
import LoginFirebase from '../components/LoginFirebase.vue'
import SignUpFirebase from '../components/SignUpFirebase.vue'
import Options from '../components/Options.vue'

Vue.use(Router)

let router = new Router({
  routes: [
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/login',
        name: 'Login',
        component: LoginFirebase
    },
    {
        path: '/signup',
        name: 'Signup',
        component: SignUpFirebase
    },
    {
        path: '/options',
        name: 'Options',
        component: Options,
        meta: {
            requireAuth: true
        }
    }
  ]
})

router.beforeEach((to, from, next) => {
    
    let currentUser = firebase.auth().currentUser
    window.currentUser = currentUser
    let requireAuth = to.matched.some(record => record.meta.requireAuth)
    console.log(currentUser);
    console.log(requireAuth);
    
    if(requireAuth && !currentUser) next('login')
    else if (!requireAuth && currentUser) next('options')
    else next()

})

export default router
