import Vue from 'vue'
import Router from 'vue-router'

import BookMarkManager from '../components/BookMarkManager.vue'
import AddBookMark from '../components/AddBookMark.vue'

Vue.use(Router)

let router = new Router({
  routes: [
    {
        path: '/',
        name: 'default',
        component: BookMarkManager
    },
    {
        path: '/bookmarks',
        name: 'BookMarks',
        component: BookMarkManager
    },
    {
        path: '/addbook',
        name: 'AddBookMark',
        component: AddBookMark
    }
  ]
})

export default router
