import Vue from 'vue'
import root from './root.vue'
import router from './router'
import firebase from '../backend/firebase.js'
import Mixins from './mixins.js';
import Filter from './filters.js';
import vueFire from 'vuefire'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.config.productionTip = false

Vue.mixin(Mixins)
Vue.use(ElementUI)
Vue.use(vueFire)

/* eslint-disable no-new */
new Vue({
    el: '#root',
    router,
    render: h => h(root)
})
