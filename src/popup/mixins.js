var mixin = {
    methods: {
        redirectPage(url) {
            chrome.tabs.create({ 'url': url })
        }
    }
}

export default mixin
